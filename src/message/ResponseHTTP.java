package message;

public class ResponseHTTP {

    public String crearMensajeResponseHTTP(String stringEco, int numCookie) {
        String datos = new String("<html> <body> <h3> El usuario ha pedido: " + stringEco + " </h3> "
                + "<h3> El numero de cookies es: " + numCookie + " </h3> </body> <html>");

        int tamFichero = datos.length();

        return "HTTP/1.1 200 OK \r\nContent-Length: " + tamFichero + "\r\nSet-Cookie: numCookie=" + numCookie
                + "\r\n\r\n" + datos;
    }

}
